import re

file = open("data/1704510811.txt", "r")
lines = file.readlines()
lines = [line.strip() for line in lines if line.strip()]
for line in lines:
    pattern = r'\(\"(\d+)\"\)'
    matches = re.findall(pattern, line)
    if matches:
        print(matches[0])