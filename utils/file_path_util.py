import time


def get_file_path(prefix):
    current_timestamp = int(time.time())
    new_file_path = "data/" + "" + prefix + "_" + str(current_timestamp) + "" + ".txt"
    return new_file_path
