import redis

from utils.file_path_util import get_file_path

# 创建Redis连接
r = redis.Redis(host='localhost', port=6379, db=0, password="wafer123")

map = r.hgetall("city_code")
code_list = []
for item in map.items():
    s = str(item[1], 'utf-8')
    code_list.append(s)

file_path = get_file_path("code_city")
file = open(file_path, "w")
for code in code_list:
    file.write(code + "\n")
