#!/usr/bin/python
import time

file = open("data/msg_enum.txt", "r")
lines = file.readlines()
new_lines = []
for line in lines:
    line = line.strip()
    if "/**" not in line and "*/" not in line and line != "":
        line = line.replace("*", "").replace("//", "").strip()
        new_lines.append(line + "\n")

current_timestamp = int(time.time())
new_file_path = "data/" + "" + str(current_timestamp) + "" + ".txt"
file1 = open(new_file_path, "w")

file1.writelines(new_lines)