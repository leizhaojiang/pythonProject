file = open('data/msg_enum.txt', 'r')
lines = file.readlines()

# 使用列表推导式过滤掉空字符串
filtered_list = [item for item in lines if item != "" and not item.startswith("/**")]


for filtered in filtered_list:
    print(filtered)
