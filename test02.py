list = range(0, 10)


def add(x):
    return x + x


lis1 = map(add, list)


# decorator
def log(func):
    def warpper(*args, **kv):
        print(f"call {func.__name__}")
        return func(*args, **kv)

    return warpper


@log
def now():
    print("aaaaaaa")


if __name__ == '__main__':
    now()
